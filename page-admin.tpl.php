<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <!--[if IE 8]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie8.css";</style>
<![endif]-->
      <!--[if IE 7]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie7.css";</style>
<![endif]-->
<!--[if IE 6]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie6.css";</style>
<![endif]-->
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
  
<script type="text/javascript">
   if (($.browser.msie == true)){
$(document).ready( function(){
  $('#sidebar-left .block, #right-column .block, #footer .block, #content, #mission, #footer-message').corners("5px transparent");

});
} 
</script>
   <!--[if lt IE 7]>
<script defer type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/pngfix.js"></script>
<![endif]-->
  
</head>

<body class="<?php print $body_classes; ?>">

<div id="admin-wrapper">

    <div id="header">
	
	   <?php if (!empty($breadcrumb)): ?><div id="breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
	 
	  
	  	    <?php if (!empty($secondary_links)): ?>
          <div class="secondary" >
            <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
          </div><!-- /second navigation -->
        <?php endif; ?>
	  
      <div id="logo-title">

    <?php if (!empty($logo)): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>

        <div id="site-name">

		<?php if (!empty($site_name)): ?>
            <h1>
            <?php print $site_name; ?>
            </h1>
          <?php endif; ?>
		
        </div> 
		
      </div> <!-- /logo-title -->

          <?php if (!empty($primary_links)): ?>
          <div id="primary" class="clear-block">
          <div id="inner-primary">
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
          </div></div>
        <?php endif; ?><!-- /primary-links -->

    </div> <!-- /header -->

        <div id="sidebar-left" class="column sidebar">
		
		       <?php if (!empty($site_slogan)): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div><!-- /site-slogan -->
          <?php endif; ?>

			  <?php if (!empty($search_box)): ?>
        <div id="search-box"><?php print $search_box; ?></div><!-- /search-box -->
      <?php endif; ?>
		
		
 <?php print $left; ?>
 
        </div> <!-- /sidebar-left -->
	
      <div id="container">
         <?php if (!empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>

        <div id="content"> 
		
		<?php if (!empty($title)): ?><h2 class="title" id="page-title"><?php print $title; ?></h2><?php endif; ?>
          <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php if (!empty($messages)): print $messages; endif; ?>
          <?php if (!empty($help)): print $help; endif; ?>
        
            <?php print $content; ?>
			
		 </div> <!-- /content -->
		 
		  <?php if (!empty($right)): ?>
        <div id="right-column" class="column sidebar">
          <?php print $right; ?>
        </div> <!-- /sidebar-right -->
      <?php endif; ?>
		

    </div> <!-- /container -->


	  <?php if (!empty($secondary_links)): ?>
          <div id="secondary" >
          <div id="inner-secondary">
            <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
          </div><!-- /second navigation -->
          </div>
        <?php endif; ?>
		 
    <div id="footer-wrapper">
	
      <div id="footer">
	  
	   <?php if (!empty($footer_left)): ?>
          <div id="footer-one" >
          <?php print $footer_left; ?>
        </div> <!-- /footer-one -->
      <?php endif; ?>
	  
	   <?php if (!empty($footer_middle)): ?>
          <div id="footer-two" >
          <?php print $footer_middle; ?>
        </div> <!-- /footer-two -->
      <?php endif; ?>
	  
 <?php if (!empty($footer_right)): ?>
          <div id="footer-three" >
          <?php print $footer_right; ?>
        </div> <!-- /footer-three -->
      <?php endif; ?>
	 
		</div>
		
    </div> <!-- /footer-wrapper -->

	    <div id="footer-message">
	  <?php print $footer_message; ?>
</div><!-- /footer-message -->

  <?php print $closure; ?>
  </div> <!-- /wrapper -->
 
</body>
</html>